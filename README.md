This is Python code that first clusters the microRNAs according to a cluster-assignment function provided by the user, for example, the microRNA family the microRNAs belong to. It then does a parallel kernel SVM, specifically cascade SVM [1]. 

[1] Hans Peter Graf, Eric Cosatto, Leon Bottou, Igor Durdanovic, Vladimir Vapnik, "Parallel Support Vector Machines: The Cascade SVM," NIPS 2005. 


Contact:
Prof. Somali Chaterji (schaterji@purdue.edu)
Asish Ghoshal (asish.geek@gmail.com)
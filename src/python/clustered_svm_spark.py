"""
    Clusters data by some criteria and learns a separate SVM model
    for each cluster. Each SVM is leanred by using the Cascading approach.
"""
from cascade_svm_spark import CascadeSvmSpark
import copy
import threading
import numpy as np


TID = 0  # Thread ids. Used during thread creation. So no mutex required
POINTS_PER_PARTITION = 10000.0


class ThreadedExecutor(threading.Thread):

    def __init__(self, func, *args, **kw_args):
        global TID
        super(ThreadedExecutor, self).__init__()
        self.tid = TID
        TID += 1
        self.func = func
        self.args = args
        self.kw_args = kw_args

    def run(self):
        print "Thread %d started running" % self.tid
        self.return_val = self.func(*self.args, **self.kw_args)
        print "Thread %d finished running" % self.tid


def predict_partition(itr, models, cluster_assignment_func):
    for x in itr:
        c_id = cluster_assignment_func(x)
        if c_id < 0:
            continue
        yield (x, models[c_id].predict(x)[0])


def predict_proba_partition(itr, models, cluster_assignment_func):
    for x in itr:
        c_id = cluster_assignment_func(x)
        if c_id < 0:
            continue
        yield (x, models[c_id].predict_proba(x)[0])


class ClusterSvmModel(object):

    def __init__(self, num_clusters, cluster_assignment_func,
                 C=1.0, prob=False, **kernel_params):
        if not callable(cluster_assignment_func):
            raise Exception("Cluster assignment function is not callable")

        self._num_clusters = num_clusters
        self._cluster_assignment_func = cluster_assignment_func
        self._C = C
        self._compute_prob = prob
        self._kernel_params = kernel_params
        self._models = [None] * num_clusters

    def train(self, sc, rdd, iterations=1, use_threads=False):

        # Copy cluster assignment function to local variable
        # so that we don't ship the entire object to the remote machines.
        func = self._cluster_assignment_func
        rdd = rdd.map(lambda x: (func(x), x))

        for i in range(self._num_clusters):
            data_rdd = rdd.filter(lambda (c_id, x): i == c_id)\
                .map(lambda (c_id, x): x)

            # Make sure that the data in each partition is repartitioned
            # so that we have sufficient number of data points to learn
            # an SVM for in each partition.
            cluster_size = data_rdd.count()
            print "Size of cluster %d is: %d" % (i, cluster_size)
            if cluster_size < POINTS_PER_PARTITION:
                num_partitions = 2
            else:
                num_partitions = int(np.ceil(cluster_size/POINTS_PER_PARTITION))
            print "Using %d number of partitions" % num_partitions
            data_rdd = data_rdd.repartition(num_partitions)
            if use_threads:
                self._models[i] = ThreadedExecutor(
                    CascadeSvmSpark.train,
                    sc, data_rdd, C=self._C, prob=self._compute_prob,
                    to_record_array=False, iterations=iterations,
                    **self._kernel_params
                )
            else:
                self._models[i] = CascadeSvmSpark.train(
                    sc, data_rdd, C=self._C, prob=self._compute_prob,
                    to_record_array=False, iterations=iterations,
                    **self._kernel_params
                )

        if use_threads:
            for i in range(self._num_clusters):
                self._models[i].start()
                self._models[i].join()

            for i in range(self._num_clusters):
                self._models[i] = self._models[i].return_val

        self._support_vectors = {}
        for i in range(self._num_clusters):
            # Make the individual models somewhat lightweight so that
            # we can ship them to the remote machines during prediction.
            self._support_vectors[i] = copy.deepcopy(
                self._models[i].get_support_vectors())
            del self._models[i].support_vectors_

    def predict(self, rdd, num_partitions=10):
        """
            Given a RDD of Record instances, returns (x, prediction(x))
        """
        for m in self._models:
            if m is None:
                raise Exception("Found a None model. Make sure the model has"
                                " been trained.")

        new_rdd = rdd.repartition(num_partitions)
        cluster_assignment_func = self._cluster_assignment_func
        models = self._models
        new_rdd = new_rdd.mapPartitions(
            lambda itr: predict_partition(itr, models, cluster_assignment_func)
        )
        return new_rdd

    def predict_proba(self, rdd, num_partitions=10):
        """
            Given a RDD of Record instances, returns (x, probability(x))
        """
        for m in self._models:
            if m is None:
                raise Exception("Found a None model. Make sure the model has"
                                " been trained.")

        new_rdd = rdd.repartition(num_partitions)
        cluster_assignment_func = self._cluster_assignment_func
        models = self._models
        new_rdd = new_rdd.mapPartitions(
            lambda itr: predict_proba_partition(itr, models,
                                                cluster_assignment_func)
        )
        return new_rdd

    def get_support_vectors(self):
        """
            Returns all support vectors across all models.
        """
        return self._support_vectors
